import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import PartyinfoS1 from "../components/partyinfo-s1";
import PartyinfoS2 from "../components/partyinfo-s2";
import PartyinfoS3 from "../components/partyinfo-s3";
import PartyinfoS4 from "../components/partyinfo-s4";

class Partyinfo extends Component{
    render(){
        return(        
            <Container fluid className="minh-footer-adj p-0">
                <PartyinfoS1/>
                <PartyinfoS2/>
                <PartyinfoS3/>
                <PartyinfoS4/>
            </Container>    
        );
    }
}

export default Partyinfo;